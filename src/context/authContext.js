import React, { useState } from 'react';
import { getCurrentUserToken, removeCurrentUserToken } from '../services/AuthDataService';
import PropTypes from 'prop-types';

export const AuthContext = React.createContext({});

export const AuthProvider = ({ children }) => {
  AuthProvider.propTypes = {
    children: PropTypes.func,
  };

  const unauthorizedUser = {
    userId: '',
    userRole: '',
    isAuth: false,
  };

  const initialSession = () => {
    const currentUserToken = getCurrentUserToken();
    if (currentUserToken) {
      return {
        userId: JSON.parse(atob(currentUserToken.token.split('.')[1])).user_id,
        userRole: JSON.parse(atob(currentUserToken.token.split('.')[1])).role,
        isAuth: true,
      };
    } else {
      return unauthorizedUser;
    }
  };

  const [currentUserSession, setCurrentUserSession] = useState(initialSession);

  const setSession = () => {
    const currentUserToken = getCurrentUserToken();
    const extractTokenObject = (jwt) => {
      try {
        return JSON.parse(atob(jwt.token.split('.')[1]));
      } catch (error) {
        // ignore
      }
    };
    if (currentUserToken) {
      setCurrentUserSession({
        ...currentUserSession,
        userId: extractTokenObject(currentUserToken).user_id,
        userRole: extractTokenObject(currentUserToken).role,
        isAuth: true,
      });
    }
  };

  const removeSession = () => {
    setCurrentUserSession({
      ...currentUserSession,
      userId: '',
      userRole: '',
      isAuth: false,
    });
    removeCurrentUserToken();
  };

  return (
    <AuthContext.Provider value={{ currentUserSession, setSession, removeSession }}>{children}</AuthContext.Provider>
  );
};
