import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getCurrentTicketFromStorage, removeCurrentTicketFromLocalStorage } from '../services/TicketDataService';

export const TicketContext = React.createContext({});

export const TicketProvider = ({ children }) => {
  TicketProvider.propTypes = {
    children: PropTypes.func,
  };

  const noTicket = {
    ticketId: '',
    isPresent: false,
  };

  const initialTicket = () => {
    const currentTicket = getCurrentTicketFromStorage();
    if (currentTicket) {
      return {
        ticketId: currentTicket,
        isPresent: true,
      };
    } else {
      return noTicket;
    }
  };

  const [currentTicket, setCurrentTicket] = useState(initialTicket);

  const setTicket = () => {
    const ticket = getCurrentTicketFromStorage();
    if (ticket) {
      setCurrentTicket({
        ...currentTicket,
        ticketId: ticket,
        isPresent: true,
      });
    }
  };

  const removeTicket = () => {
    setCurrentTicket({
      ...currentTicket,
      ticketId: '',
      isPresent: false,
    });
    removeCurrentTicketFromLocalStorage();
  };

  return <TicketContext.Provider value={{ currentTicket, setTicket, removeTicket }}>{children}</TicketContext.Provider>;
};
