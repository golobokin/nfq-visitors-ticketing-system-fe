import { BE_ENDPOINTS, RESPONSE_STATUS } from '../components/constants';
import axiosInstance from './axios-base';

const login = (username, password) => {
  return axiosInstance
    .post(BE_ENDPOINTS.LOG_IN, {
      username,
      password,
    })
    .then((response) => {
      if (response.data.token && response.status === RESPONSE_STATUS.OK) {
        localStorage.setItem('jwt', JSON.stringify(response.data));
      }
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const removeCurrentUserToken = () => {
  localStorage.removeItem('jwt');
};

const getCurrentUserToken = () => {
  return JSON.parse(localStorage.getItem('jwt'));
};

export { login, removeCurrentUserToken, getCurrentUserToken };
