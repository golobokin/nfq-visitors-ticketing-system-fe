import axiosInstance from './axios-base';
import { useState, useEffect } from 'react';
import { MESSAGES_ERROR } from '../components/messages';
import { DATA_REFRESH_INTERVAL, RESPONSE_STATUS } from '../components/constants';

function useFetchDataWithInterval(urlPath) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsloading] = useState(true);
  const [reload, setReload] = useState(true);

  useEffect(() => {
    const interval = setInterval(() => setReload((state) => !state), DATA_REFRESH_INTERVAL);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    axiosInstance
      .get(urlPath)
      .then((res) => {
        setData(res.data);
        setIsloading(false);
      })
      .catch((error) => {
        console.log(error.response);
        if (!error.response) {
          setError(MESSAGES_ERROR.SERVER_NOT_RESPONDING);
        } else if (
          error.response.status === RESPONSE_STATUS.FORBIDDEN ||
          error.response.status === RESPONSE_STATUS.UNAUTHORIZED
        ) {
          setError(MESSAGES_ERROR.UNAUTHORIZED);
        } else if (error.response.status === RESPONSE_STATUS.CONFLICT) {
          setError(error.response.data.message);
        } else {
          setError(MESSAGES_ERROR.BAD_REQUEST);
        }
        setIsloading(false);
      });
  }, [urlPath, reload]);

  return { data, error, isLoading };
}

export default useFetchDataWithInterval;
