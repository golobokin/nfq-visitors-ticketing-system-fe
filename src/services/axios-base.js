import axios from 'axios';
import { API_URL } from '../components/constants';

const axiosInstance = axios.create({
  baseURL: API_URL + '/api',
});

axiosInstance.interceptors.request.use(
  (config) => {
    const jwt = JSON.parse(localStorage.getItem('jwt'));
    if (jwt) {
      config.headers.authorization = 'Bearer ' + jwt.token;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export default axiosInstance;
