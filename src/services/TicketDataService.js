import axiosInstance from './axios-base';
import { BE_ENDPOINTS, RESPONSE_STATUS } from '../components/constants';

const issueTicket = (specialistId) => {
  return axiosInstance
    .get(BE_ENDPOINTS.ISSUE_TICKET + specialistId)
    .then((response) => {
      if (response.data.id && response.status === RESPONSE_STATUS.OK) {
        localStorage.setItem('ticket', response.data.id);
      }
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const cancelTicket = (id) => {
  return axiosInstance
    .post(BE_ENDPOINTS.CANCEL_TICKET + id)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const startTicket = (id) => {
  return axiosInstance
    .post(BE_ENDPOINTS.MARK_TICKET_START_TIME + id)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const endTicket = (id) => {
  return axiosInstance
    .post(BE_ENDPOINTS.MARK_TICKET_END_TIME + id)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const removeCurrentTicketFromLocalStorage = () => {
  localStorage.removeItem('ticket');
};

const getCurrentTicketFromStorage = () => {
  return localStorage.getItem('ticket');
};

export {
  issueTicket,
  cancelTicket,
  startTicket,
  endTicket,
  getCurrentTicketFromStorage,
  removeCurrentTicketFromLocalStorage,
};
