import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { AuthProvider } from './context/authContext';
import * as serviceWorker from './serviceWorker';
import { TicketProvider } from './context/ticketContext';

ReactDOM.render(
  <BrowserRouter>
    <AuthProvider>
      <TicketProvider>
        <App />
      </TicketProvider>
    </AuthProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

serviceWorker.unregister();
