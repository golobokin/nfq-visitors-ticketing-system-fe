import React, { useContext } from 'react';
import { PAGE_LINKS } from './components/constants';
import { Switch } from 'react-router-dom';
import Homepage from './components/Home/Home';
import CustomerHomePage from './components/Customer/CustomerHome';
import ProtectedRoute from './components/Routes/ProtectedRoute';
import SpecialistHomePage from './components/Specialist/SpecialistHome';
import LogIn from './components/LogIn/LogIn';
import { AuthContext } from './context/authContext';
import DepartmentScreen from './components/DepartmentScreen/DepartmentScreen';
import AdminRoute from './components/Routes/AdminRoute';
import PublicOnlyRoute from './components/Routes/PublicOnlyRoute';
import SelectSpecialist from './components/Customer/SelectSpecialist';
import GetTicket from './components/Customer/GetTicket';
import SpecialistAppointments from './components/Specialist/Appointments/Appointments';
import { TicketContext } from './context/ticketContext';
import PublicRouteWithTicket from './components/Routes/PublicRouteWithTicket';

function App() {
  const { currentUserSession } = useContext(AuthContext);
  const isAuth = currentUserSession.isAuth;
  const userRole = currentUserSession.userRole;
  const { currentTicket } = useContext(TicketContext);
  const isPresent = currentTicket.isPresent;

  return (
    <Switch>
      <PublicOnlyRoute path={PAGE_LINKS.HOME} exact component={Homepage} auth={isAuth} />
      <PublicOnlyRoute path={PAGE_LINKS.CUSTOMER} exact component={CustomerHomePage} auth={isAuth} />
      <PublicRouteWithTicket
        path={PAGE_LINKS.CUSTOMER_SELECT_SPEC}
        exact
        component={SelectSpecialist}
        auth={isAuth}
        ticket={isPresent}
      />
      <PublicOnlyRoute path={PAGE_LINKS.CUSTOMER_GET_TICKET} exact component={GetTicket} auth={isAuth} />
      <PublicOnlyRoute path={PAGE_LINKS.LOG_IN} exact component={LogIn} auth={isAuth} />
      <ProtectedRoute path={PAGE_LINKS.SPECIALIST} exact component={SpecialistHomePage} auth={isAuth} role={userRole} />
      <ProtectedRoute
        path={PAGE_LINKS.SPECIALIST_SCREEN}
        exact
        component={SpecialistAppointments}
        auth={isAuth}
        role={userRole}
      />
      <AdminRoute path={PAGE_LINKS.DEPT_SCREEN} exact component={DepartmentScreen} auth={isAuth} role={userRole} />
    </Switch>
  );
}

export default App;
