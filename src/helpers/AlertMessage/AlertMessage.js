import Collapse from '@material-ui/core/Collapse';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  alert: {
    width: '100%',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
}));

export default function AlertMessage(props) {
  const classes = useStyles();
  const { isOpen, message, messageStyle } = props;
  const [open, setOpen] = useState(isOpen);

  AlertMessage.propTypes = {
    isOpen: PropTypes.bool,
    message: PropTypes.string,
    messageStyle: PropTypes.string,
  };

  return (
    <div className={classes.alert}>
      <Collapse in={open}>
        <Alert
          severity={messageStyle}
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          {message}
        </Alert>
      </Collapse>
    </div>
  );
}
