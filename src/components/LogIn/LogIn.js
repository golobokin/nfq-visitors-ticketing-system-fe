import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { login } from '../../services/AuthDataService';
import { RESPONSE_STATUS, REDIRECT_DELAY } from '../constants';
import { MESSAGES_ERROR, MESSAGES_SUCCESS } from '../messages';
import AlertMessage from '../../helpers/AlertMessage/AlertMessage';
import { AuthContext } from '../../context/authContext';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  paper: {
    paddingTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LogIn() {
  const { setSession } = useContext(AuthContext);
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [loginMessage, setLoginMessage] = useState('');
  const [signInMessageStyle, setSignInMessageStyle] = useState('success');
  const [values, setValues] = useState({
    username: '',
    password: '',
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    login(values.username, values.password)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.USER_LOGIN_SUCCESS);
          setTimeout(() => setSession(), REDIRECT_DELAY);
        }
      })
      .catch((error) => {
        setOpen(true);
        if (!error.response) {
          setSignInMessageStyle('error');
          setLoginMessage(MESSAGES_ERROR.SERVER_NOT_RESPONDING);
        } else if (
          error.response.status === RESPONSE_STATUS.NOT_FOUND ||
          error.response.status === RESPONSE_STATUS.UNAUTHORIZED
        ) {
          setSignInMessageStyle('warning');
          setLoginMessage(MESSAGES_ERROR.WRONG_CREDENTIALS);
        }
      });
  };

  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        <Grid className={classes.paper}>
          <Typography component="h1" variant="h5">
            Please enter your credentials
          </Typography>
          {loginMessage && <AlertMessage isOpen={open} message={loginMessage} messageStyle={signInMessageStyle} />}
          <form onSubmit={handleSubmit} autoComplete="off">
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              required
              id="username"
              label="Username"
              name="username"
              autoFocus
              value={values.username}
              onChange={handleChange('username')}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              required
              name="password"
              label="Password"
              type="password"
              id="password"
              value={values.password}
              onChange={handleChange('password')}
            />
            <Button type="submit" fullWidth variant="contained" color="inherit" className={classes.submit}>
              Log In
            </Button>
          </form>
        </Grid>
      </Container>
    </>
  );
}
