import { isLocalhost } from '../serviceWorker';

export const API_URL = isLocalhost ? 'http://localhost:8080' : 'https://evts.herokuapp.com';

export const PAGE_LINKS = {
  HOME: '/',
  CUSTOMER: '/customer',
  CUSTOMER_SELECT_SPEC: '/customer/select',
  CUSTOMER_GET_TICKET: '/customer/ticket',
  SPECIALIST: '/specialist',
  SPECIALIST_SCREEN: '/specialist_screen',
  DEPT_SCREEN: '/department_screen',
  LOG_IN: '/login',
};

export const ROLE = {
  SPECIALIST: 'ROLE_SPECIALIST',
  ADMIN: 'ROLE_ADMIN',
};

export const RESPONSE_STATUS = {
  OK: 200,
  CREATED: 201,
  EMPTY: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  INTERNAL_SERVER_ERROR: 500,
};

export const REDIRECT_DELAY = 1200;

export const DATA_REFRESH_INTERVAL = 5000;

export const BE_ENDPOINTS = {
  LOG_IN: '/auth/login',
  AVAILABLE_SPECIALISTS: '/public/specialists',
  ISSUE_TICKET: '/public/ticket/specialist/',
  GET_TICKET_INFO: '/public/ticket/',
  CANCEL_TICKET: '/public/ticket/cancel/',
  MARK_TICKET_START_TIME: '/ticket/start/',
  MARK_TICKET_END_TIME: '/ticket/end/',
  GET_SPECIALIST_APPOINTMENTS: '/tickets/specialist/',
  GET_NEXT_5_APPOINTMENTS: '/tickets/5/specialist/',
  GET_CURRENT_APPOINTMENT: '/tickets/current/specialist/',
};
