import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import SpecialistDash from './SpecialistDash/SpecialistDash';
import useFetchSpecialists from '../../services/DataFetchService';
import { BE_ENDPOINTS } from '../constants';
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import { Person } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  container: {
    width: 280,
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(1),
  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: theme.spacing(1),
  },
}));

function DepartmentScreen() {
  const classes = useStyles();
  const { data, error, isLoading } = useFetchSpecialists(BE_ENDPOINTS.AVAILABLE_SPECIALISTS);

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <>
          <Container>
            <Grid container className={classes.root} spacing={1}>
              {data.map((specialist) => (
                <Grid item className={classes.paper} key={specialist.id}>
                  <Paper className={classes.container}>
                    <Person className={classes.icon} />
                    <Typography variant="h5" align="center" gutterBottom>
                      {specialist.type}
                    </Typography>
                  </Paper>
                  <SpecialistDash specialistId={specialist.id} />
                </Grid>
              ))}
            </Grid>
          </Container>
        </>
      );
    }
  };
  return isLoading ? <LinearProgress /> : content();
}

export default DepartmentScreen;
