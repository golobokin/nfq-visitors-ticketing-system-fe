import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { BE_ENDPOINTS } from '../../constants';
import { Typography } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import useFetchNextAppointments from '../../../services/DataFetchWithIntervalService';
import CountdownClock from '../../CountdownClock/CountdownClock';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import { ConfirmationNumber } from '@material-ui/icons';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  container: {
    width: 280,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(1),
  },
  table: {
    width: '100%',
  },
}));

function NextTickets(props) {
  const classes = useStyles();
  // eslint-disable-next-line react/prop-types
  const { specialistId } = props;
  const { data, error, isLoading } = useFetchNextAppointments(BE_ENDPOINTS.GET_NEXT_5_APPOINTMENTS + specialistId);

  const parseDate = (postdate) => {
    moment.locale('lt');
    return moment(postdate).format('HH:mm');
  };

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <>
          <Paper className={classes.container}>
            <Typography variant="h5" align="center" gutterBottom>
              Next ticket in:
            </Typography>
            {data[0] && <CountdownClock endTime={data[0].visitBookedTime} id={data[0].id} />}
            {!data[0] && (
              <Typography variant="h5" align="center" gutterBottom>
                No upcoming tickets!
              </Typography>
            )}
          </Paper>
          <TableContainer component={Paper} className={classes.container}>
            <Table stickyHeader className={classes.table} aria-label="simple table">
              <TableBody>
                {data.map((visit) => (
                  <TableRow hover key={visit.id}>
                    <TableCell>
                      <ConfirmationNumber />
                    </TableCell>
                    <TableCell align="center">{visit.ticketNumber}</TableCell>
                    <TableCell align="center">{parseDate(visit.visitBookedTime)}</TableCell>
                    <TableCell align="center">{visit.isCancelled && 'Cancelled'}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default NextTickets;
