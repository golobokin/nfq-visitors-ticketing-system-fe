import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import useFetchCurrentAppointment from '../../../services/DataFetchWithIntervalService';
import { BE_ENDPOINTS } from '../../constants';
import { Typography } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles((theme) => ({
  container: {
    width: 280,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(1),
  },
}));

function CurrentTicket(props) {
  const classes = useStyles();
  // eslint-disable-next-line react/prop-types
  const { specialistId } = props;
  const { data, error, isLoading } = useFetchCurrentAppointment(BE_ENDPOINTS.GET_CURRENT_APPOINTMENT + specialistId);
  // eslint-disable-next-line react/prop-types

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <Paper item className={classes.container}>
          <Typography variant="h5" align="center" gutterBottom>
            Current ticket number: {data.ticketNumber}
          </Typography>
        </Paper>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default CurrentTicket;
