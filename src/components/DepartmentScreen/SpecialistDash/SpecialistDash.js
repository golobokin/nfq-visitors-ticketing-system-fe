import React from 'react';
import CurrentTicket from './CurrentTicket';
import NextTickets from './NextTickets';

function SpecialistDash(props) {
  // eslint-disable-next-line react/prop-types
  const { specialistId } = props;

  return (
    <>
      <CurrentTicket specialistId={specialistId} />
      <NextTickets specialistId={specialistId} />
    </>
  );
}

export default SpecialistDash;
