import React, { useContext, useState } from 'react';
import useGetTicket from '../../services/DataFetchService';
import { BE_ENDPOINTS, PAGE_LINKS, REDIRECT_DELAY, RESPONSE_STATUS } from '../constants';
import Container from '@material-ui/core/Container';
import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Cancel, ConfirmationNumber } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import CountdownClock from '../CountdownClock/CountdownClock';
import { useHistory } from 'react-router-dom';
import { MESSAGES_ERROR, MESSAGES_SUCCESS } from '../messages';
import AlertMessage from '../../helpers/AlertMessage/AlertMessage';
import { cancelTicket } from '../../services/TicketDataService';
import LinearProgress from '@material-ui/core/LinearProgress';
import moment from 'moment';
import { TicketContext } from '../../context/ticketContext';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(6),
  },
  paper: {
    height: 220,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  cancel: {
    height: 40,
    width: 200,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  icon: {
    height: 60,
    width: 60,
    marginBottom: theme.spacing(2),
  },
  heading: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(4),
  },
}));

function GetTicket() {
  const { removeTicket, currentTicket } = useContext(TicketContext);
  const ticketId = currentTicket.ticketId;
  const classes = useStyles();
  const history = useHistory();
  // eslint-disable-next-line react/prop-types
  const { data, error, isLoading } = useGetTicket(BE_ENDPOINTS.GET_TICKET_INFO + ticketId);
  const [open, setOpen] = useState(false);
  const [loginMessage, setLoginMessage] = useState('');
  const [signInMessageStyle, setSignInMessageStyle] = useState('success');

  const getVisitTime = () => {
    moment.locale('lt');
    return moment(data.visitBookedTime).format('HH:mm');
  };

  const cancelThisTicket = () => {
    cancelTicket(data.id)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.TICKET_CANCEL_SUCCESS);
          setTimeout(() => history.push(PAGE_LINKS.CUSTOMER), REDIRECT_DELAY);
          setTimeout(() => removeTicket(), REDIRECT_DELAY);
        }
      })
      .catch((error) => {
        setOpen(true);
        if (!error.response) {
          setSignInMessageStyle('error');
          setLoginMessage(MESSAGES_ERROR.SERVER_NOT_RESPONDING);
        } else if (
          error.response.status === RESPONSE_STATUS.NOT_FOUND ||
          error.response.status === RESPONSE_STATUS.BAD_REQUEST
        ) {
          setSignInMessageStyle('warning');
          setLoginMessage(MESSAGES_ERROR.BAD_REQUEST);
        }
      });
  };

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Box className={classes.heading}>
            <Typography component="h1" variant="h5">
              {error}
            </Typography>
          </Box>
        </Container>
      );
    } else {
      return (
        <>
          <Container maxWidth="md">
            {loginMessage && <AlertMessage isOpen={open} message={loginMessage} messageStyle={signInMessageStyle} />}
            <Box className={classes.heading}>
              <Typography component="h1" variant="h5">
                Your ticket is ready!
              </Typography>
            </Box>
            <Grid container className={classes.root} spacing={2}>
              <Grid item>
                <Paper className={classes.paper}>
                  <ConfirmationNumber className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Ticket number
                  </Typography>
                  <Typography variant="h4" align="center" gutterBottom>
                    {data.ticketNumber}
                  </Typography>
                </Paper>
              </Grid>
              <Grid item>
                <Paper className={classes.paper}>
                  <ConfirmationNumber className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Your booked time
                  </Typography>
                  <Typography variant="h4" align="center" gutterBottom>
                    {getVisitTime()}
                  </Typography>
                </Paper>
              </Grid>
              <Grid item>
                <Paper className={classes.paper}>
                  <ConfirmationNumber className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Time left until visit
                  </Typography>
                  <Typography variant="h4" align="center" gutterBottom>
                    <CountdownClock endTime={data.visitBookedTime} id={data.id} />
                  </Typography>
                </Paper>
              </Grid>
              <Grid item onClick={cancelThisTicket} style={{ cursor: 'pointer' }}>
                <Paper className={classes.cancel}>
                  <Cancel className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Cancel ticket
                  </Typography>
                </Paper>
              </Grid>
            </Grid>
          </Container>
        </>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default GetTicket;
