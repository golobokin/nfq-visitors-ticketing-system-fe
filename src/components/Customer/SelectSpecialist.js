import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { PersonAdd } from '@material-ui/icons';
import useFetchSpecialists from '../../services/DataFetchService';
import { BE_ENDPOINTS, REDIRECT_DELAY, RESPONSE_STATUS } from '../constants';
import Box from '@material-ui/core/Box';
import LinearProgress from '@material-ui/core/LinearProgress';
import { MESSAGES_ERROR, MESSAGES_SUCCESS } from '../messages';
import { issueTicket } from '../../services/TicketDataService';
import AlertMessage from '../../helpers/AlertMessage/AlertMessage';
import { TicketContext } from '../../context/ticketContext';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(6),
  },
  paper: {
    height: 280,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  icon: {
    height: 180,
    width: 180,
    marginBottom: theme.spacing(2),
  },
  heading: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(4),
  },
}));

function SelectSpecialist() {
  const classes = useStyles();
  const history = useHistory();

  const { setTicket, currentTicket } = useContext(TicketContext);
  const isPresent = currentTicket.isPresent;

  const { data, error, isLoading } = useFetchSpecialists(BE_ENDPOINTS.AVAILABLE_SPECIALISTS);

  const [open, setOpen] = useState(false);
  const [loginMessage, setLoginMessage] = useState('');
  const [signInMessageStyle, setSignInMessageStyle] = useState('success');

  const handleIssueTicket = (specialistId) => {
    isPresent ? getAlreadyIssuedTicket() : issueNewTicket(specialistId);
  };

  const getAlreadyIssuedTicket = () => {
    history.push('/customer/ticket');
  };

  const issueNewTicket = (specialistId) => {
    issueTicket(specialistId)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.TICKET_ISSUED_SUCCESS);
          setTimeout(() => setTicket(), REDIRECT_DELAY);
        }
      })
      .catch((error) => {
        setOpen(true);
        if (!error.response) {
          setSignInMessageStyle('error');
          setLoginMessage(MESSAGES_ERROR.SERVER_NOT_RESPONDING);
        } else if (
          error.response.status === RESPONSE_STATUS.NOT_FOUND ||
          error.response.status === RESPONSE_STATUS.UNAUTHORIZED
        ) {
          setSignInMessageStyle('warning');
          setLoginMessage(MESSAGES_ERROR.WRONG_CREDENTIALS);
        }
      });
  };

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <>
          <Container maxWidth="md">
            {loginMessage && <AlertMessage isOpen={open} message={loginMessage} messageStyle={signInMessageStyle} />}
            <Box className={classes.heading}>
              <Typography component="h1" variant="h5">
                Select specialist to get a ticket
              </Typography>
            </Box>
            <Grid container className={classes.root} spacing={2}>
              {data.map((value) => (
                <Grid key={value.id} item onClick={() => handleIssueTicket(value.id)} style={{ cursor: 'pointer' }}>
                  <Paper className={classes.paper}>
                    <PersonAdd className={classes.icon} />
                    <Typography variant="h4" align="center" gutterBottom>
                      {value.type}
                    </Typography>
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </Container>
        </>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default SelectSpecialist;
