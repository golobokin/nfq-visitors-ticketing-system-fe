import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { ConfirmationNumber } from '@material-ui/icons';
import { PAGE_LINKS } from '../constants';
import { Link } from 'react-router-dom';
import { TicketContext } from '../../context/ticketContext';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(6),
  },
  paper: {
    height: 280,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  icon: {
    height: 180,
    width: 180,
    marginBottom: theme.spacing(2),
  },
}));

function CustomerHomePage() {
  const classes = useStyles();
  const { currentTicket } = useContext(TicketContext);
  const isTicketPresent = currentTicket.isPresent;

  return (
    <>
      <Container maxWidth="md">
        <Grid container className={classes.root} spacing={2}>
          {!isTicketPresent && (
            <Grid item>
              <Link to={PAGE_LINKS.CUSTOMER_SELECT_SPEC}>
                <Paper className={classes.paper}>
                  <ConfirmationNumber className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Reserve a ticket
                  </Typography>
                </Paper>
              </Link>
            </Grid>
          )}
          {isTicketPresent && (
            <Grid item>
              <Link to={PAGE_LINKS.CUSTOMER_GET_TICKET}>
                <Paper className={classes.paper}>
                  <ConfirmationNumber className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    See your reservation
                  </Typography>
                </Paper>
              </Link>
            </Grid>
          )}
        </Grid>
      </Container>
    </>
  );
}

export default CustomerHomePage;
