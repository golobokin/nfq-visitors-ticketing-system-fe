import React, { useContext } from 'react';
import { AuthContext } from '../../../context/authContext';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import moment from 'moment';
import useFetchNextAppointments from '../../../services/DataFetchWithIntervalService';
import { BE_ENDPOINTS } from '../../constants';
import { Typography } from '@material-ui/core';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import { ConfirmationNumber } from '@material-ui/icons';
import CountdownClock from '../../CountdownClock/CountdownClock';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(() => ({
  table: {
    width: '100%',
  },
  container: {
    maxHeight: 'fullHeight',
  },
}));

function SpecialistAppointmentsTable() {
  const classes = useStyles();
  const { currentUserSession } = useContext(AuthContext);
  const { data, error, isLoading } = useFetchNextAppointments(
    BE_ENDPOINTS.GET_SPECIALIST_APPOINTMENTS + currentUserSession.userId
  );

  const parseDate = (postdate) => {
    moment.locale('lt');
    return moment(postdate).format('HH:mm');
  };

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <>
          <Paper className={classes.container}>
            <Typography variant="h4" align="center" gutterBottom>
              Time until next appointment:
            </Typography>
            {data[0] && <CountdownClock endTime={data[0].visitBookedTime} id={data[0].id} />}
            {!data[0] && (
              <Typography variant="h4" align="center" gutterBottom>
                No upcoming appointments!
              </Typography>
            )}
          </Paper>
          <TableContainer component={Paper} className={classes.container}>
            <Table stickyHeader className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell width="10px"> </TableCell>
                  <TableCell align="center">Ticket number</TableCell>
                  <TableCell align="center">Visit time</TableCell>
                  <TableCell align="center" width="120px">
                    {' '}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((visit) => (
                  <TableRow hover key={visit.id}>
                    <TableCell>
                      <ConfirmationNumber />
                    </TableCell>
                    <TableCell align="center">{visit.ticketNumber}</TableCell>
                    <TableCell align="center">{parseDate(visit.visitBookedTime)}</TableCell>
                    <TableCell align="center">{visit.isCancelled && 'Cancelled'}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default SpecialistAppointmentsTable;
