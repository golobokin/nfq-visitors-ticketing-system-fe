import React, { useContext, useState } from 'react';
import { AuthContext } from '../../../context/authContext';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import useFetchCurrentAppointment from '../../../services/DataFetchWithIntervalService';
import { BE_ENDPOINTS, RESPONSE_STATUS } from '../../constants';
import { Typography } from '@material-ui/core';
import SpecialistAppointmentsTable from './AppointmentTable';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Cancel, PlayCircleOutline, Stop } from '@material-ui/icons';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { cancelTicket, endTicket, startTicket } from '../../../services/TicketDataService';
import { MESSAGES_ERROR, MESSAGES_SUCCESS } from '../../messages';
import AlertMessage from '../../../helpers/AlertMessage/AlertMessage';

const useStyles = makeStyles((theme) => ({
  grid: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: theme.spacing(1),
  },
  container: {
    maxHeight: 'fullHeight',
    paddingTop: theme.spacing(1),
  },
  icon: {
    height: 25,
    width: 25,
    margin: theme.spacing(1),
  },
}));

function SpecialistAppointments() {
  const classes = useStyles();
  const { currentUserSession } = useContext(AuthContext);
  const { data, error, isLoading } = useFetchCurrentAppointment(
    BE_ENDPOINTS.GET_CURRENT_APPOINTMENT + currentUserSession.userId
  );
  const [open, setOpen] = useState(false);
  const [loginMessage, setLoginMessage] = useState('');
  const [signInMessageStyle, setSignInMessageStyle] = useState('success');

  const setOnResponseError = (error) => {
    if (!error.response) {
      setSignInMessageStyle('error');
      setLoginMessage(MESSAGES_ERROR.SERVER_NOT_RESPONDING);
    } else if (
      error.response.status === RESPONSE_STATUS.NOT_FOUND ||
      error.response.status === RESPONSE_STATUS.BAD_REQUEST
    ) {
      setSignInMessageStyle('warning');
      setLoginMessage(MESSAGES_ERROR.BAD_REQUEST);
    }
  };

  const handleCancel = () => {
    cancelTicket(data.id)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.TICKET_CANCEL_SUCCESS);
        }
      })
      .catch((error) => {
        setOpen(true);
        setOnResponseError(error);
      });
  };

  const handleStart = () => {
    startTicket(data.id)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.TICKET_MARK_START_SUCCESS);
        }
      })
      .catch((error) => {
        setOpen(true);
        setOnResponseError(error);
      });
  };

  const handleEnd = () => {
    endTicket(data.id)
      .then((response) => {
        if (response.status === RESPONSE_STATUS.OK) {
          setOpen(true);
          setSignInMessageStyle('success');
          setLoginMessage(MESSAGES_SUCCESS.TICKET_MARK_END_SUCCESS);
        }
      })
      .catch((error) => {
        setOpen(true);
        setOnResponseError(error);
      });
  };

  const content = () => {
    if (error !== null) {
      return (
        <Container align="center" maxWidth="md">
          <Typography variant="h6" color="textSecondary" gutterBottom>
            {error}
          </Typography>
        </Container>
      );
    } else {
      return (
        <>
          <Container>
            {loginMessage && <AlertMessage isOpen={open} message={loginMessage} messageStyle={signInMessageStyle} />}
            <Paper className={classes.container}>
              <Typography variant="h3" align="center" gutterBottom>
                Current ticket number: {data.ticketNumber}
              </Typography>
              <Grid container className={classes.grid}>
                {data && !data.hasTicketStarted && !data.hasTicketEnded && !data.isCancelled && (
                  <Grid item>
                    <Button onClick={handleStart}>
                      <PlayCircleOutline className={classes.icon} /> Start appointment
                    </Button>
                  </Grid>
                )}
                {data && data.hasTicketStarted && !data.hasTicketEnded && !data.isCancelled && (
                  <Grid item>
                    <Button onClick={handleEnd}>
                      <Stop className={classes.icon} /> End appointment
                    </Button>
                  </Grid>
                )}
                {data && !data.isCancelled && !data.hasTicketStarted && (
                  <Grid item>
                    <Button onClick={handleCancel}>
                      <Cancel className={classes.icon} /> Cancel appointment
                    </Button>
                  </Grid>
                )}
                {data && data.isCancelled && (
                  <Grid item>
                    <Typography variant="h4" align="center" gutterBottom>
                      This appointment was cancelled. Have a cup of coffee!
                    </Typography>
                  </Grid>
                )}
                {data && data.hasEnded && (
                  <Grid item>
                    <Typography variant="h4" align="center" gutterBottom>
                      This appointment ended
                    </Typography>
                  </Grid>
                )}
              </Grid>
            </Paper>
            <SpecialistAppointmentsTable />
          </Container>
        </>
      );
    }
  };

  return isLoading ? <LinearProgress /> : content();
}

export default SpecialistAppointments;
