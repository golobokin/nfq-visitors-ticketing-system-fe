import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { Dvr, List, Lock } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { PAGE_LINKS } from '../constants';
import { AuthContext } from '../../context/authContext';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(6),
  },
  paper: {
    height: 280,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  icon: {
    height: 180,
    width: 180,
    marginBottom: theme.spacing(2),
  },
  heading: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(4),
  },
}));

function SpecialistHomePage() {
  const classes = useStyles();
  const { removeSession, currentUserSession } = useContext(AuthContext);
  const isAuth = currentUserSession.isAuth;
  const userRole = currentUserSession.userRole;

  return (
    <>
      <Container maxWidth="md">
        <Box className={classes.heading}>
          <Typography component="h1" variant="h5">
            You are logged in
          </Typography>
        </Box>
        <Grid container className={classes.root} spacing={2}>
          {isAuth && userRole === 'ROLE_SPECIALIST' && (
            <Grid item>
              <Link to={PAGE_LINKS.SPECIALIST_SCREEN}>
                <Paper className={classes.paper}>
                  <List className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Your next appointments
                  </Typography>
                </Paper>
              </Link>
            </Grid>
          )}
          {isAuth && userRole === 'ROLE_ADMIN' && (
            <Grid item>
              <Link to={PAGE_LINKS.DEPT_SCREEN}>
                <Paper className={classes.paper}>
                  <Dvr className={classes.icon} />
                  <Typography variant="h4" align="center" gutterBottom>
                    Screen
                  </Typography>
                </Paper>
              </Link>
            </Grid>
          )}
          <Grid item>
            <Link to={PAGE_LINKS.SPECIALIST} onClick={removeSession}>
              <Paper className={classes.paper}>
                <Lock className={classes.icon} />
                <Typography variant="h4" align="center" gutterBottom>
                  Log Out
                </Typography>
              </Paper>
            </Link>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default SpecialistHomePage;
