import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { PAGE_LINKS } from '../constants';
import PropTypes from 'prop-types';

PublicRouteWithTicket.propTypes = {
  auth: PropTypes.bool,
  component: PropTypes.func,
  role: PropTypes.string,
  ticket: PropTypes.bool,
};

function PublicRouteWithTicket({
  auth: isAuth,
  role: userRole,
  component: Component,
  ticket: isTicketPresent,
  ...rest
}) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuth && !isTicketPresent) {
          return <Component {...props} />;
        } else {
          return <Redirect to={PAGE_LINKS.CUSTOMER_GET_TICKET} />;
        }
      }}
    />
  );
}

export default PublicRouteWithTicket;
