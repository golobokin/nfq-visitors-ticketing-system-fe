import React from 'react';
import { Route } from 'react-router-dom';
import { ROLE } from '../constants';
import PropTypes from 'prop-types';
import LogIn from '../LogIn/LogIn';

ProtectedRoute.propTypes = {
  auth: PropTypes.bool,
  component: PropTypes.func,
  role: PropTypes.string,
};

function ProtectedRoute({ auth: isAuth, role: userRole, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={() => {
        if (isAuth && (userRole === ROLE.SPECIALIST || userRole === ROLE.ADMIN)) {
          return <Component />;
        } else {
          return <LogIn />;
        }
      }}
    />
  );
}

export default ProtectedRoute;
