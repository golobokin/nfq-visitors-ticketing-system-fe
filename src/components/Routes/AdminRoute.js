import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { PAGE_LINKS, ROLE } from '../constants';
import PropTypes from 'prop-types';

AdminRoute.propTypes = {
  auth: PropTypes.bool,
  component: PropTypes.func,
  role: PropTypes.string,
};

function AdminRoute({ auth: isAuth, role: userRole, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={() => {
        if (isAuth && userRole === ROLE.ADMIN) {
          return <Component />;
        } else {
          return <Redirect to={PAGE_LINKS.LOG_IN} />;
        }
      }}
    />
  );
}

export default AdminRoute;
