import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { PAGE_LINKS } from '../constants';
import PropTypes from 'prop-types';

PublicOnlyRoute.propTypes = {
  auth: PropTypes.bool,
  component: PropTypes.func,
  role: PropTypes.string,
};

function PublicOnlyRoute({ auth: isAuth, role: userRole, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuth) {
          return <Component {...props} />;
        } else {
          return <Redirect to={PAGE_LINKS.SPECIALIST} />;
        }
      }}
    />
  );
}

export default PublicOnlyRoute;
