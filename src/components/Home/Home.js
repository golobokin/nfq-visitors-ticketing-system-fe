import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { Person, SupervisorAccount } from '@material-ui/icons';
import { PAGE_LINKS } from '../constants';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(6),
  },
  paper: {
    height: 280,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    padding: theme.spacing(5, 5),
  },
  icon: {
    height: 180,
    width: 180,
    marginBottom: theme.spacing(2),
  },
}));

function Homepage() {
  const classes = useStyles();

  return (
    <>
      <Container maxWidth="md">
        <Grid container className={classes.root} spacing={2}>
          <Grid item>
            <Link to={PAGE_LINKS.CUSTOMER}>
              <Paper className={classes.paper}>
                <Person className={classes.icon} />
                <Typography variant="h4" align="center" gutterBottom>
                  Customer
                </Typography>
              </Paper>
            </Link>
          </Grid>
          <Grid item>
            <Link to={PAGE_LINKS.SPECIALIST}>
              <Paper className={classes.paper}>
                <SupervisorAccount className={classes.icon} />
                <Typography variant="h4" align="center" gutterBottom>
                  Specialist
                </Typography>
              </Paper>
            </Link>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default Homepage;
