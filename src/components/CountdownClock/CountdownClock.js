import { Typography } from '@material-ui/core';
import React from 'react';
import Countdown from 'react-countdown';

function CountdownClock(props) {
  // eslint-disable-next-line react/prop-types
  const { endTime } = props;

  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      return <>Now!</>;
    } else {
      return (
        <span>
          {hours < 10 ? '0' + hours : hours}:{minutes < 10 ? '0' + minutes : minutes}:
          {seconds < 10 ? '0' + seconds : seconds}
        </span>
      );
    }
  };

  return (
    <Typography variant="h4" align="center" gutterBottom>
      <Countdown date={new Date(endTime)} renderer={renderer} />
    </Typography>
  );
}

export default CountdownClock;
