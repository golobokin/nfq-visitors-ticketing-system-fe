export const MESSAGES_ERROR = {
  SERVER_NOT_RESPONDING: 'Connection error. Please try again later.',
  BAD_REQUEST: 'Error processing your request.',
  WRONG_CREDENTIALS: 'Incorrect username or password. Please try again.',
  SERVER_ERROR: 'Server error.',
  UNAUTHORIZED: 'You have no rights to see this information.',
};
export const MESSAGES_SUCCESS = {
  USER_LOGIN_SUCCESS: 'Login successful.',
  TICKET_CANCEL_SUCCESS: 'Ticket cancelled.',
  TICKET_ISSUED_SUCCESS: ' Ticket issued.',
  TICKET_MARK_START_SUCCESS: 'Appointment marked as started.',
  TICKET_MARK_END_SUCCESS: 'Appointment marked as ended.',
};
